# GitCode 帮助文档

欢迎来到GitCode！GitCode致力于为广大开发者提供一个开放、协作、自由的编程环境，探索开源世界的无限可能。

为了帮助你更好地使用GitCode，我们特地为你准备了这份产品使用说明文档。在接下来的内容中，我们将详细介绍如何使用GitCode的主要功能以及常见问题解决方法。

## 一、注册账号

1. 打开GitCode官方网站(https://gitcode.com)，点击页面右上角“注册”按钮，根据引导完成注册即可。

## 二、功能概述

### 1. 发布和分享代码
	* 在网站创建一个新的代码仓库
	* 将你的代码提交到该仓库中
	* 通过分享链接让其他人访问你的代码 
### 2. 参与讨论和交流
	* 在项目的issue内，反馈关于代码的问题或者想法
    * 在项目的讨论内，可以参与回复、投票等活动，与他人交流
    * 在组织社区内，可以了解该组织最新活动、内容推荐，了解组织的最新动向。
### 3. 协作编程
	* 参与他人的项目，共同解决问题或开发新功能
	* 通过Pull Request向项目提交你的修改和建议
### 4. 阅读和学习他人代码
	* 通过搜索找到感兴趣的项目或代码仓库
	* 查看项目的源代码、文档和贡献者信息

## 三、常见问题及解决方法

**1. 无法连接到服务器** 

*  检查网络连接是否正常，尝试重新启动客户端或更换网络环境
* 确认你的账户是否存在异常，如有需要请联系GitCode客服（ kefu@gitcode.com ）支持

**2. 无法创建项目**
* 检查你的账户权限是否正确
* 确认你的账户是否存在异常，如有需要请联系GitCode客服（ kefu@gitcode.com ）支持

**3. 无法提交代码到仓库**
* 检查你的提交信息是否填写正确，包括提交说明、文件内容和分支选择等
* 确认你的账户权限是否正确，如有需要请联系GitCode客服（ kefu@gitcode.com ）支持

## 四、详细操作文档


- [组织](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/组织.md)
    - [新建组织](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/组织/新建组织.md)
    - [组织入驻](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/组织/组织入驻.md)
    - [组织社区](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/组织/组织社区.md)
- [项目](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目.md)
    - [新建项目](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/新建项目.md)
    - [导入项目](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/导入项目.md)
- [查找文件](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/查找文件.md)
- [代码](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/代码.md)
    - [分支](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/分支.md)
    - [提交](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/提交.md)
    - [文件编辑](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/编辑.md)
    - [Git 属性](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/Git属性.md)
    - [Blame](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/Blame.md)
    - [文件历史记录](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/文件历史记录.md)
    - [保护分支](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/保护分支.md)
    - [保护 Tags](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/保护Tags.md)
    - [Git LFS](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/LFS.md)
    - [Tags](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/Tags.md)
    - [发行版](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/Release.md)
- [合并请求](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求.md)
    - [Fork 工作流](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/fork.md)
    - [创建合并请求](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/创建合并请求.md)
    - [草稿](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/草稿.md)
    - [代码评审](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/代码评审.md)
    - [解决冲突](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/解决冲突.md)
    - [合并请求版本](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/版本.md)
    - [Squash 合并](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/Squash.md)
    - [快进合并](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/快进合并.md)
    - [Cherry Pick](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/CherryPick.md)
    - [Revert](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/合并请求/Revert.md)
- [项目成员](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/成员.md)
- [项目设置](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/设置.md)
- [Wiki](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/wiki.md)
- [Issue](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/Issue.md)
    - [私密 Issue](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/私密Issue.md)
    - [操作 Issue](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/操作Issue.md)
    - [Label](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/Label.md)
    - [里程碑](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/项目/里程碑.md)
- [讨论](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/讨论.md)
- [搜索](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/搜索.md)
- [Markdown 语法](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/Markdown.md)
- [用户](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/用户.md)
    - [绑定邮箱](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/用户/邮箱.md)
    - [绑定 SSH 公钥](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/用户/ssh.md)
    - [个人访问密钥](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/用户/经典访问密钥.md)
    - [账号安全](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/用户/安全.md)
- [个人工作台](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/工作台.md)
- [通知](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/wiki/通知.md)